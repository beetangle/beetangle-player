CURRENT_HOSTNAME=`cat /etc/hostname | tr -d " \t\n\r"`
echo $1 > /etc/hostname
sed -i "s/127.0.1.1.*$CURRENT_HOSTNAME/127.0.1.1\t$1/g" /etc/hosts
