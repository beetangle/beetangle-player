# hide console on startup
sudo sed -i 's/console=tty1/console=tty3/g' /boot/cmdline.txt
sudo sed -i 's/$/ loglevel=3 vt.global_cursor_default=0 logo.nologo/g' /boot/cmdline.txt
sudo sed -i '/^1:2345:respawn:\/sbin\/getty/ s/^/#/' /etc/inittab