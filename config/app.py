
import hashlib

import os
import re
import time
import subprocess
import requests
from requests.exceptions import RequestException, ConnectionError
import json
import logging
import codecs

from flask import Flask
from flask import request, session
from flask import render_template
from flask import redirect, url_for, flash

from werkzeug.contrib.fixers import ProxyFix
from flask_login import (LoginManager, login_user, logout_user, login_required,
                         UserMixin)

from utils import (read_config, update_config, build_api_access_headers,
                   build_api_json_headers, init_logging, build_abs_path)

init_logging()

app = Flask('config')
app.secret_key = os.urandom(32)
app.wsgi_app = ProxyFix(app.wsgi_app)


login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
login_manager.login_message = None

DEBUG = True

BEETANGLE_ROOT = 'https://app.beetangle.com'
# BEETANGLE_ROOT = 'https://test.beetangle.com'

API_ROOT = BEETANGLE_ROOT + '/api/v1/'

DEFAULT_PASSWORD = 'admin'


@app.route('/')
@login_required
def index():
    access_token = _read_access_token()
    config = read_config()

    if not access_token or not config.get('chid'):
        return redirect(url_for('set_token'))

    playlist = _read_playlist()

    return render_template('index.html', **{
        'config': config,
        'playlist': playlist,
    })


@app.route('/downloader')
@login_required
def run_downloader():
    app.logger.info('Forcing downloader run')
    subprocess.Popen('python ' + build_abs_path('../downloader/downloader.py'),
                     shell=True)

    # sleep for a while such a downloader can write last_downloader_run
    # into config file and 'index' can display it correctly
    time.sleep(0.2)

    return redirect(url_for('index'))


@app.route('/player')
@login_required
def restart_player():
    app.logger.info('Forcing player restart')
    subprocess.Popen(build_abs_path('../bin/qplay'), shell=True)
    subprocess.Popen('python ' + build_abs_path('../player/player.py'), shell=True)

    return redirect(url_for('index'))


@app.route('/update')
@login_required
def update_player():

    def _get_head():
        return subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    app.logger.info('Forcing player update')

    current_head = _get_head()
    return_code = subprocess.call(['git', 'pull'])

    if return_code != 0:
        logging.error('Git pull failed (return code: %s).', return_code)
        return _redirect_to_index(error='Beetangle Player update failed.')

    new_head = _get_head()
    app.logger.info('Current HEAD: %s, new HEAD: %s', current_head, new_head)

    if current_head == new_head:
        msg = 'Beetangle Player is up-to-date (revision: %s).' % current_head
    else:
        flash('Beetangle Player was successfully updated (revision: %s).' %
              new_head)

        # prevent possible application crashes caused by invalid url_for tags
        app.url_build_error_handlers.append(lambda error, endpoint, values: '')
        return restart_device()

    return _redirect_to_index(message=msg)


@app.route('/restart')
@login_required
def restart_device():
    app.logger.info('Forcing device restart')
    subprocess.call(['sudo', 'shutdown', '-r', 'now'])

    return _redirect_to_index(error='Device is restarting...')


@app.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():
    if request.method == 'POST':
        settings_dict = {
            'audio': request.form.get('audio')
        }
        update_config(settings_dict)
        settings_str = ','.join((k + '=' + v for k, v in
                                 settings_dict.iteritems()))
        app.logger.info('Setting saved (%s).', settings_str)
        msg = 'Settings updated. Restart the playout for applying changes'
        return _redirect_to_index(message=msg)

    config = read_config()
    return render_template('settings.html', **config)


@app.route('/set_device', methods=['GET', 'POST'])
@login_required
def set_device():
    access_token = _read_access_token()
    if not access_token:
        redirect(url_for('index'))

    device_name = request.form.get('devname')
    error = None

    if request.method == 'POST' and not device_name:
        error = 'Please enter device name bellow.'
    elif request.method == 'POST' and device_name:    
        if not re.match("^[A-Za-z0-9-]+$", device_name): 
            error = 'Device name can contain only a-z/A-Z characters, numbers and hyphen.'

    if request.method == 'POST' and not error:
        data = {'type': 'tv', 'name': device_name}
        try:
            ch = requests.post(API_ROOT + 'channels/',
                               data=json.dumps(data),
                               headers=build_api_json_headers(access_token),
                               verify=False)
            ch.raise_for_status()
        except ConnectionError, ex:
            app.logger.exception(ex)
            error = 'Cannot reach server. Is device connected to the internet?'
        except RequestException, ex:
            app.logger.exception(ex)
            error = str(ex)
        else:
            channel = ch.json()

            _write_channel_id(channel['id'])
            update_config({
                'chid': channel['id'],
                'channel_name': channel['name']
            })

            app.logger.info('Channel \'%s\' successfully created (id=%s)',
                            channel['name'], channel['id'])

            _switch_console_visibility(False)
            _change_hostname(channel['name'])

            if _is_partition_extended():
                return render_template('finish_config_reboot.html')
            else:
                return render_template('finish_config.html')

    return render_template('device.html', **{
        'error': error,
        'device_name': device_name,
    })


@app.route('/extend_root_partition')
@login_required
def extend_root_partition():
    _extend_root_partition()
    return redirect(url_for('restart_device'))


@app.route('/set_token', methods=['GET', 'POST'])
@login_required
def set_token():
    access_token = request.form.get('access_token')
    error = None

    if request.method == 'POST':
        # verify token by request to users/me/
        try:
            me = requests.post(API_ROOT + 'users/me/',
                               headers=build_api_access_headers(access_token),
                               verify=False)
            if me.status_code == 401:
                error = 'Invalid token passed.'
                app.logger.warning(error)
            else:
                me.raise_for_status()
        except ConnectionError, ex:
            app.logger.exception(ex)
            error = 'Cannot reach server. Is device connected to the internet?'
        except RequestException, ex:
            app.logger.exception(ex)
            error = str(ex)

        if not error:
            _write_access_token(access_token)

            me = me.json()
            update_config({
                'team': me['team'],
                'username': me['login'],
            })

            app.logger.info('Token for user \'%s\' in \'%s\' team successfully '
                            'saved', me['login'], me['team'])

            return redirect(url_for('set_device'))

    return render_template('get_token.html', **{
        'access_token': access_token,
        'error': error,
        'BEETANGLE_ROOT': BEETANGLE_ROOT,
    })


@app.route('/disconnect')
@login_required
def disconnect():
    access_token = _read_access_token()
    if not access_token:
        return redirect(url_for('index'))

    config = read_config()
    channel_id = config['chid']
    error = None

    try:
        ch = requests.delete(API_ROOT + 'channels/' + str(channel_id) + '/',
                             headers=build_api_access_headers(access_token),
                             verify=False)
        ch.raise_for_status()
    except RequestException, ex:
        app.logger.exception(ex)
        error = str(ex)

    _delete_access_token()
    _delete_channel_id()
    update_config({
        'chid': '',
        'channel_name': ''
    })
    _delete_playlist()
    _switch_console_visibility(True)
    app.logger.info('Channel id=%s deleted', channel_id)

    return _redirect_to_index(error=error)


@app.route('/logs')
@login_required
def show_logs():
    log = _read_log()
    if not log:
        log = 'Error reading log file!'

    return render_template('logs.html', **{
        'log_dump': log,
        'config': read_config(),
    })



@app.errorhandler(500)
def custom_500_handler(error):
    log = _read_log()
    return render_template('500.html', log_dump=log), 500


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if AdminUser.check_password(request.form.get('password')):
            login_user(AdminUser())
            return redirect(request.args.get('next') or url_for('index'))
        else:
            error = 'Incorrect password'

    return render_template('login.html', error=error)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/password',  methods=['GET', 'POST'])
@login_required
def change_password():
    error = None
    if request.method == 'POST':
        form = request.form
        if AdminUser.check_password(form.get('password')):
            if form.get('new_password1') == form.get('new_password2'):
                _write_password(form.get('new_password1'))
                return _redirect_to_index(message='Your password was changed.')
            else:
                error = 'New password check does not match'
        else:
            error = 'Incorrect current password'

    return render_template('change_password.html', error=error)


class AdminUser(UserMixin):
    ID = 1

    def get_id(self):
        return self.ID

    @classmethod
    def check_password(cls, raw_password):
        return _hash_password(raw_password) == _read_password()


@login_manager.user_loader
def load_user(user_id):
    return AdminUser() if user_id == AdminUser.ID else None


def _redirect_to_index(message=None, error=None):
    if message:
        flash(message)
    if error:
        flash(error, category='error')
    return redirect(url_for('index'))


def _read_access_token():
    access_token = session.get('access_token')
    if access_token:
        return access_token
    else:
        try:
            with open(build_abs_path('token'), 'r') as token_file:
                access_token = token_file.read()
            if access_token:
                session['access_token'] = access_token
                return access_token
            else:
                return ''
        except IOError:
            return ''


def _write_access_token(access_token):
    with open(build_abs_path('token'), 'w') as f:
        f.write(access_token)
    session['access_token'] = access_token


def _delete_access_token():
    with open(build_abs_path('token'), 'w') as token_file:
        token_file.seek(0)
        token_file.truncate()
    session.pop('access_token', None)


def _write_channel_id(channel_id):
    with open(build_abs_path('chid'), 'w') as f:
        f.write(str(channel_id))


def _read_playlist():
    try:
        with open(build_abs_path('../player/clips/playlist.json')) as json_playlist:
            return json.load(json_playlist)
    except IOError:
        return []
    except ValueError:
        return []

def _delete_playlist():
    try:
        with open(build_abs_path('../player/clips/playlist.json'), 'w') as json_playlist:
            json_playlist.seek(0)
            json_playlist.truncate()
    except IOError:
        pass


def _read_log():
    log_path = build_abs_path('../logs/beetangle.log')
    try:
        with codecs.open(log_path, encoding='utf-8') as f:
            return f.read()
    except IOError:
        return None


def _delete_channel_id():
    with open(build_abs_path('chid'), 'w') as chid_file:
        chid_file.seek(0)
        chid_file.truncate()


def _read_password():
    try:
        with open('passwd') as f:
            return f.read()
    except IOError:
        return _hash_password(DEFAULT_PASSWORD)


def _write_password(raw_password):
    with open('passwd', 'w') as f:
        f.write(_hash_password(raw_password))


def _hash_password(raw_password):
    return hashlib.sha256(raw_password).hexdigest()

def _switch_console_visibility(on_off):
    if on_off:
        app.logger.info('Switching console visibility on')
        subprocess.Popen(build_abs_path('../bin/show_console.sh'), shell=True)
    else:
        app.logger.info('Switching console visibility off')
        subprocess.Popen(build_abs_path('../bin/hide_console.sh'), shell=True)

def _change_hostname(hostname):
    app.logger.info('Changing Pi hostname to: ' + hostname)
    subprocess.Popen('sudo ' + build_abs_path('../bin/change_hostname.sh') + ' ' + hostname, shell=True)


def _is_partition_extended():
    try:
        with open('extended') as f:
            return True
    except IOError:
        return False

def _mark_partition_extended():
    with open('extended', 'w') as f:
        f.write('extended')

def _extend_root_partition():
    app.logger.info('Extending root partition to full SD card capacity')
    subprocess.Popen('sudo ' + build_abs_path('../bin/expand1_fdisk.sh'), shell=True)
    subprocess.Popen('sudo ' + build_abs_path('../bin/expand2_initd.sh'), shell=True)
    _mark_partition_extended()



if __name__ == '__main__':
    logging.getLogger('werkzeug').setLevel(logging.WARNING)

    app.logger.info('Starting app ...')
    app.run(host='0.0.0.0', debug=DEBUG)
