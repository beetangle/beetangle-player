#!/bin/bash
set -e

# setting a log file
# TODO: make more generic path
LOGFILE=/home/pi/beetangle-player/logs/gunicorn.log
LOGDIR=$(dirname $LOGFILE)

#number of workers. A popular formula is 1 + 2 * number_of_cpus on the machine
NUM_WORKERS=1

# address to listen to
ADDRESS=0.0.0.0:8001

cd /home/pi/beetangle-player/config
# activating the virtualenv
# TODO: make more generic path
source /home/pi/.virtualenvs/player/bin/activate

# create log dir if it doesn't exist
test -d $LOGDIR || mkdir -p $LOGDIR

# run gunicorn server 
exec gunicorn -w $NUM_WORKERS --bind=$ADDRESS \
  --log-level=debug \
    --log-file=$LOGFILE 2>>$LOGFILE \
      app:app